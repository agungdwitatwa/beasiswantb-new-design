import "style/App.scss";
import Header from "components/Header/Header";
import Hero from "components/Section/HeroSection/Hero";

function App() {
  return (
    <div className="container">
      <Header />
      <Hero />
    </div>
  );
}

export default App;
