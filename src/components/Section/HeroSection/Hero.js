import "./style/hero.scss";

export default function Hero() {
  return (
    <div className="hero">
      <div className="image">
        <img src="/hero.png" alt="Hero Beasiswa" />
      </div>
      <div className="text">
        <h2>Beasiswa NTB</h2>
        <p>
          Beasiswa NTB adalah program unggulan Pemerintah Provinsi Nusa Tenggara
          Barat yang bertujuan untuk meningkatkan kualitas sumber daya manusia
          yang ada di daerah Nusa Tenggara Barat
        </p>
        <a className="button" href="#open">
          Selengkapnya
        </a>
      </div>
    </div>
  );
}
