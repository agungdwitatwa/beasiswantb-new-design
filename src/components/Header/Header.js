import "./style/header.scss";

export default function Header() {
  return (
    <div className="header">
      <div className="logo">
        <div className="image">
          <img src="/logo.png" alt="logo Beasiswa" />
        </div>
        <h1 className="text">Beasiswa NTB</h1>
      </div>
      <div className="menu">
        <ul>
          <li>
            <a href="#beranda">Beranda</a>
          </li>
          <li>
            <a href="#beranda">Jenis Beasiswa</a>
          </li>
          <li>
            <a href="#beranda">Sebaran Beasiswa</a>
          </li>
          <li>
            <a href="#beranda">Persyaratan Umum</a>
          </li>
          <li>
            <a href="#beranda">Berita Seputar</a>
          </li>
        </ul>
      </div>
    </div>
  );
}
